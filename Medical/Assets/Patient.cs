﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Patient : MonoBehaviour
{
    private float _health;
    private float _energy = 0;
    private float _maxHealth;
    [SerializeField] private HealthBar _healthBar;
    [SerializeField] private Text _energyText;

    [SerializeField] private GameObject _energyObject;
    [SerializeField] private GameObject _energyObject2;
    [SerializeField] private GameObject _energyObject3;
    [SerializeField] private bool _stopSpawning;
    [SerializeField] private float _spawnTime;
    [SerializeField] private float _spawnDelay;
    [SerializeField] private OrganManager[] _organs = new OrganManager[2];
    [SerializeField] private GameObject _winPanel;
    [SerializeField] private GameObject _losePanel;

    private bool SFX_playing = false;
    private AudioSource SFX_heartRate;
    private AudioSource SFX_Objective;
    private bool SFX_GameOver;

    public float Energy { get => _energy; set => _energy = value; }

  /// <summary>
  /// Start is called on the frame when a script is enabled just before
  /// any of the Update methods is called the first time.
  /// </summary>
    void Start()
    {
        SFX_GameOver = false;
        foreach(OrganManager organ in _organs)
        {
            if (organ != null)
                _maxHealth += organ.MaxHealth;
        }
        _healthBar.MaxHealth = _maxHealth;

        InvokeRepeating("SpawnObject", _spawnTime, _spawnDelay);
        InvokeRepeating("SpawnObject2", 3, 3);
        InvokeRepeating("SpawnObject3", 5, 5);

    }

    /// <summary>
    /// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    /// </summary>
    void FixedUpdate()
    {
        _health = GetHealth();
        _healthBar.Health = _health;
        _energyText.text = _energy.ToString();
        Debug.Log(_health);

        // Heart Rate Sound Effects

        if(_health >= _maxHealth * 60 / 100 && _health < _maxHealth)
            SFX_heartRate = GameObject.Find("HeartNormal").GetComponent<AudioSource>();
        else if(_health >= _maxHealth * 30 / 100 && _health < _maxHealth * 60 / 100)
            SFX_heartRate = GameObject.Find("HeartSlow").GetComponent<AudioSource>();
        else if(_health >= 1 && _health < _maxHealth * 30 / 100)
            SFX_heartRate = GameObject.Find("HeartEmergency").GetComponent<AudioSource>();
        else if(_health < 0)
            SFX_heartRate = GameObject.Find("HeartDead").GetComponent<AudioSource>();

        if(!SFX_playing && _health > 0 && _health < _maxHealth)
            StartCoroutine("playHeartRate");
        else if (_health < 0){
            SFX_heartRate.Stop();
            SFX_heartRate.Play();
        }

        // cek win lose condition
        if (_health >= _maxHealth){
            SFX_GameOver = true;
            if(SFX_GameOver){
                SFX_Objective = GameObject.Find("WinSFX").GetComponent<AudioSource>();
                SFX_Objective.Play();
            }
           
            _winPanel.SetActive(true);
        }
        else if (_health <= 0){
            SFX_GameOver = true;
            if(SFX_GameOver){
                SFX_Objective = GameObject.Find("LoseSFX").GetComponent<AudioSource>();
                SFX_Objective.Play();
            }
            _losePanel.SetActive(true);
        }
    }
    private float GetHealth()
    {
        float health = 0;
        
        foreach(OrganManager organ in _organs)
        {
            if (organ != null)
                health += organ.Health;
        }

        return health;
    }

    public void BadHabit(float damage)
    {
        foreach(OrganManager organ in _organs)
        {
            if (organ != null && organ.Health < 100)
                organ.Health -= damage;
        }
    }

    public void addEnergy(float amount)
    {
        _energy += amount;
    }

    public void reduceEnergy(float amount)
    {
        float energyLeft = _energy -= amount;
        if (energyLeft <= 0)
            _energy = 0;
        else
            _energy = energyLeft;
    }


    public void SpawnObject()
    {
        Vector2 newPos = new Vector2(Random.Range(-4, 4),Random.Range(-2, 2));
        GameObject newEnergy = Instantiate(_energyObject, newPos, Quaternion.identity);
        newEnergy.transform.SetParent(transform, false);
        if (_stopSpawning) {
            CancelInvoke("SpawnObject");
        }
    }


    public void SpawnObject2()
    {
        Vector2 newPos = new Vector2(Random.Range(-4, 4),Random.Range(-2, 2));
        GameObject newEnergy = Instantiate(_energyObject2, newPos, Quaternion.identity);
        newEnergy.transform.SetParent(transform, false);
        if (_stopSpawning) {
            CancelInvoke("SpawnObject");
        }
    }


    public void SpawnObject3()
    {
        Vector2 newPos = new Vector2(Random.Range(-4, 4),Random.Range(-2, 2));
        GameObject newEnergy = Instantiate(_energyObject3, newPos, Quaternion.identity);
        newEnergy.transform.SetParent(transform, false);
        if (_stopSpawning) {
            CancelInvoke("SpawnObject");
        }
    }

    IEnumerator playHeartRate(){
        SFX_playing = true;
        SFX_heartRate.Play();

        yield return new WaitForSeconds(SFX_heartRate.clip.length);
        SFX_playing = false;
    }
}
