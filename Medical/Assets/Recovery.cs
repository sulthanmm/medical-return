﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Recovery : MonoBehaviour
{
    private OrganManager _organ;
    [SerializeField] private GameObject _detailPanel;
    [SerializeField]private Patient _patient;

    private Button _btn;
    private Image _image;

    public OrganManager Organ { get => _organ; set => _organ = value; }

  /// <summary>
  /// Start is called on the frame when a script is enabled just before
  /// any of the Update methods is called the first time.
  /// </summary>
    void Start()
    {
        _btn = gameObject.GetComponent<Button>();
        _image = gameObject.GetComponent<Image>();
        _btn.onClick.AddListener(Recover);
    }
    public void Recover()
    {
        Care _care = _detailPanel.GetComponent<DisplayDetailPanel>().Care;
        float recovery = _organ.MaxHealth * (_care.EnergyPercentage / 100);
        Debug.Log("max health " + _organ.MaxHealth);
        Debug.Log("health " + _organ.Health);
        Debug.Log("recover " + recovery);

        _organ.Recovery(recovery);
        _patient.GetComponent<Patient>().reduceEnergy(_care.Energy);
        StartCoroutine(_care.CooldownTimer());
    }

    /// <summary>
    /// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    /// </summary>
    void FixedUpdate()
    {
        DisableAbility();
    }

    void DisableAbility()
    {
        Care _care = _detailPanel.GetComponent<DisplayDetailPanel>().Care;
        if (_patient.Energy < _care.Energy || _care.OnCooldown)
            _image.enabled = false;
        else if (_patient.Energy >= _care.Energy && !_care.OnCooldown)
            _image.enabled = true;
    }
}
