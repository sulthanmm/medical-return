﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneButton : MonoBehaviour
{
    public string scene;
    private AudioSource buttonSFX;
    private MusicManager m_Manager;

    void Start(){
        m_Manager = GameObject.Find("MusicManager").GetComponent<MusicManager>();
        buttonSFX = GameObject.Find("ButtonSFX").GetComponent<AudioSource>();
    }

    public void moveScene(){
        if(m_Manager.SFX == true){
           buttonSFX.Play();
        }
        SceneManager.LoadScene(scene);
    }
}
