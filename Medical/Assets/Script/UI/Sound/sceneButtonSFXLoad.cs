﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sceneButtonSFXLoad : MonoBehaviour
{
    public static sceneButtonSFXLoad sfxButton;
    private void Awake() {
        if(sfxButton == null){
            sfxButton = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else if(sfxButton != this){
            Destroy(gameObject);
        }
    }
}
