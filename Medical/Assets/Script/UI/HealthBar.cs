﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Slider Slider;

    public float MaxHealth {
        set => Slider.maxValue = value;
    }

    public float Health {
        get => Slider.value;
        set => Slider.value = value;
    }
}
