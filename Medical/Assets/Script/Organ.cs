﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Organ : MonoBehaviour
{
    [SerializeField] private OrganManager _organ = null;
    [SerializeField] private Animator _canvas = null;
    [SerializeField] private Text _nameText = null;
    [SerializeField] private HealthBar _healthBar = null;
    [SerializeField] private GameObject _healthText = null;

    [SerializeField] private GameObject _medicOptionPanel = null;
    [SerializeField] private GameObject _conventionalMedicOption = null;
    [SerializeField] private GameObject _herbalMedicOption = null;
    [SerializeField] private GameObject _selfMedicOption = null;
    [SerializeField] private GameObject _intensifMedicOption = null;
    [SerializeField] private GameObject _useMedic = null;
    
    [SerializeField] public Care[] HerbalCares = new Care[3];
    [SerializeField] public Care[] ConventionalCares = new Care[3];
    [SerializeField] public Care[] SelfCares = new Care[3];
    [SerializeField] public Care[] IntensifCares = new Care[3];

    private AudioSource SFX_Button;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    private void Start()
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = _organ.ArtWork;
        _nameText.text = _organ.Name;
        _healthBar.MaxHealth = _organ.MaxHealth;
        _healthBar.Health = _organ.Health;
        _healthText.GetComponent<Text>().text = _organ.Health + " %";
        SFX_Button = GameObject.Find("ButtonSFX").GetComponent<AudioSource>();
    }


    /// <summary>
    /// OnMouseDown is called when the user has pressed the mouse button while
    /// over the GUIElement or Collider.
    /// </summary>
    private void OnMouseDown()
    {
        _medicOptionPanel.GetComponent<MedicOption>().Organ = _organ;
        GetComponent<SpriteRenderer>().color = Color.green;
        _canvas.SetTrigger("generalstatus_close");
        _conventionalMedicOption.GetComponent<ConventionalMedicOption>().Cares = ConventionalCares;
        _herbalMedicOption.GetComponent<ConventionalMedicOption>().Cares = HerbalCares;
        _intensifMedicOption.GetComponent<ConventionalMedicOption>().Cares = IntensifCares;
        _selfMedicOption.GetComponent<ConventionalMedicOption>().Cares = SelfCares;
        SFX_Button.Play();
        _useMedic.GetComponent<Recovery>().Organ = _organ;
        _canvas.SetTrigger("medicoption_open");
    }

    /// <summary>
    /// OnMouseUp is called when the user has released the mouse button.
    /// </summary>
    private void OnMouseUp()
    {
        GetComponent<SpriteRenderer>().color = Color.white;
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        _healthBar.MaxHealth = _organ.MaxHealth;
        _healthBar.Health = _organ.Health;
        _healthText.GetComponent<Text>().text = Math.Round( _organ.Health, 1) + " %";
    }
}
