using UnityEngine;

public class Pneumonia : Care {
  /// <summary>
  /// Awake is called when the script instance is being loaded.
  /// </summary>
  void Awake()
  {
      
  }

  private void Antibiotics()
  {

  }

  private void AntiPain()
  {

  }

  private void LowDoseCoughMedicine()
  {

  }

  private void DrinkHerbalTea()
  {

  }

  private void GingerAndTurmericConsumption()
  {

  }

  private void DrinkMineralWater()
  {

  }

  private void Rest()
  {

  }

  private void StopOverWalking()
  {

  }

  private void AntibioticsIntravena()
  {

  }

  private void RespiraionTherapy()
  {

  }

  private void OralRehidrationTherapy()
  {
    
  }
}