﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConventionalMedicOption : MonoBehaviour
{
    private Care[] _cares = new Care[3];
    [SerializeField] private GameObject _carePanel;
    private Button _btn;

    public Care[] Cares { get => _cares; set => _cares = value; }



  /// <summary>
  /// Start is called on the frame when a script is enabled just before
  /// any of the Update methods is called the first time.
  /// </summary>
    void Start()
    {
        _btn = gameObject.GetComponent<Button>();
        _btn.onClick.AddListener(TaskOnClick);
    }

    private void TaskOnClick()
    {
        GameObject element0 = _carePanel.transform.GetChild(0).gameObject;
        GameObject element1 = _carePanel.transform.GetChild(1).gameObject;
        GameObject element2 = _carePanel.transform.GetChild(2).gameObject;

        if (_cares[0] != null)
            element0.GetComponent<CarePanelElement>().Care = _cares[0];
        if (_cares[1] != null)
            element1.GetComponent<CarePanelElement>().Care = _cares[1];
        if (_cares[2] != null)
        {
            element2.GetComponent<CarePanelElement>().Care = _cares[2];
            element2.SetActive(true);
        }
        else
        {
            element2.SetActive(false); 
        }
    }
}
