﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarePanelElement : MonoBehaviour
{
  private Care _care;
  [SerializeField] private Text _name;

  [SerializeField] private DisplayDetailPanel _detailPanel;
  public Care Care { get => _care; set => _care = value; }

  private Button _btn;

  /// <summary>
  /// Start is called on the frame when a script is enabled just before
  /// any of the Update methods is called the first time.
  /// </summary>
  void Update()
  {
    _btn = gameObject.GetComponent<Button>();
    _btn.onClick.AddListener(TaskOnClick);

    _name.text = _care.Name;
    gameObject.GetComponent<Image>().sprite = _care.ArtWork;
  }

  private void TaskOnClick()
  {
    _detailPanel.Care = _care;
  }
}
